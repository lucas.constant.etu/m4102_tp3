package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (idPizza INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation ( idPizza int NOT NULL,\n" +
            "    idIngredient int NOT NULL,\n" +
            "    CONSTRAINT PK_PizzaIngredient PRIMARY KEY (\n" +
            "        idPizza,\n" +
            "        idIngredient\n" +
            "    ),\n" +
            "    FOREIGN KEY (idPizza) REFERENCES Pizzas (idPizza),\n" +
            "    FOREIGN KEY (idIngredient) REFERENCES ingredients (id))")
    void createAssociationTable();

    @Transaction
    default void createTableAndIngredientAssociation() {
        createAssociationTable();
        createPizzaTable();
    }

    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropTable();

    @SqlUpdate("INSERT INTO Pizzas (name) VALUES (:name)")
    @GetGeneratedKeys
    long insert(String name);

    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Ingredient> getAll();

    @SqlQuery("SELECT * FROM Pizzas WHERE idPizzas = :id")
    @RegisterBeanMapper(Pizza.class)
    Ingredient findById(long id);

    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Ingredient findByName(String name);

    @SqlUpdate("DELETE FROM Pizzas WHERE idPizzas = :id")
    void remove(long id);
}